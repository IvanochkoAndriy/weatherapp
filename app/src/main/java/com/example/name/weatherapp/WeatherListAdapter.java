package com.example.name.weatherapp;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class WeatherListAdapter extends RecyclerView.Adapter<WeatherListAdapter.ViewHolder> {

    private List<ModelWeatherList> modelWeatherLists;
    private final OnItemClickListener listener;


    public WeatherListAdapter(List<ModelWeatherList> modelWeatherLists, OnItemClickListener listener){
        this.modelWeatherLists = modelWeatherLists;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ModelWeatherList weatherList = modelWeatherLists.get(position);

        holder.cityView.setText(weatherList.getCityName());
        holder.tempView.setText(weatherList.getTempNow());
        holder.cloudView.setText(weatherList.getCloudsNow());
        holder.windView.setText(weatherList.getWindsNow());
        holder.dateView.setText(weatherList.getDateNow());
        holder.timeView.setText(weatherList.getTimeNow());
        holder.weatherView.setText(weatherList.getWeatherNow());
        holder.imageView.setText(weatherList.getImageNow());

        holder.bind(weatherList, listener);
    }

    @Override
    public int getItemCount() {
        return modelWeatherLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView cityView;
        private TextView imageView;
        private TextView weatherView;
        private TextView timeView;
        private TextView dateView;
        private TextView cloudView;
        private TextView windView;
        private TextView tempView;

        public ViewHolder(View view) {
            super(view);

            tempView = (TextView) view.findViewById(R.id.tempListItem);
            windView = (TextView) view.findViewById(R.id.windsView);
            cloudView = (TextView) view.findViewById(R.id.cloudView);
            dateView = (TextView) view.findViewById(R.id.dateView);
            timeView = (TextView) view.findViewById(R.id.timeView);
            weatherView = (TextView) view.findViewById(R.id.weatherView);
            cityView = (TextView) view.findViewById(R.id.cityName);
            imageView = (TextView) view.findViewById(R.id.imageView);
        }


        public void bind(final ModelWeatherList item, final OnItemClickListener listener) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    public interface OnItemClickListener{
        void onItemClick(ModelWeatherList modelWeatherList);
    }
}