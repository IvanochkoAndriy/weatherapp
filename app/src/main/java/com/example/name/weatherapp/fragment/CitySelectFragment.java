package com.example.name.weatherapp.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.name.weatherapp.CitySelect;
import com.example.name.weatherapp.R;

public class CitySelectFragment extends Fragment {

    private RadioGroup cityGroup;

    private CitySelect citySelect;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        citySelect = new CitySelect(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_city_select, container, false);

        cityGroup = view.findViewById(R.id.cityGroup);

        RadioButton radioButton = (RadioButton)cityGroup.getChildAt(citySelect.getRadio());

        radioButton.setChecked(true);

        cityGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                citySelect.setRadio(cityGroup,checkedId,getActivity());
                switch (checkedId){
                    case R.id.drogobychRadio:
                        citySelect.setCityName(CitySelect.ID_DROGOBYCH);
                        break;
                    case R.id.lvivRadio:
                        citySelect.setCityName(CitySelect.ID_LVIV);
                        break;
                    case R.id.truskavetcRadio:
                        citySelect.setCityName(CitySelect.ID_TRUSKAVETC);
                        break;
                }
            }
        });
        return view;
    }

}
