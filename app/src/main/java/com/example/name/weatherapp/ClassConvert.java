package com.example.name.weatherapp;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Name on 12.12.2017.
 */

public class ClassConvert {

    public static String unixConvert(String dataSet, boolean justDate){

        Date date = new Date(Long.valueOf(dataSet)*1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf;

        if(justDate) {
            sdf = new SimpleDateFormat("d, EEE"); // the format of your date
        }else{
            sdf = new SimpleDateFormat("HH:mm"); // the format of your date
            sdf.setTimeZone(TimeZone.getTimeZone("UTC+2"));
        }
        String formattDate = sdf.format(date);

        return formattDate;
    }

    public static int tempKelvinConvert(String tempKelvin){

        int tempCelsium = 0;

        double convert = Double.valueOf(tempKelvin) - 273.15;

        tempCelsium = (int) convert;

        return tempCelsium;
    }

}
