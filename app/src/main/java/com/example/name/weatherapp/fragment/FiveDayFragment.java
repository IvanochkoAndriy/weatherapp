package com.example.name.weatherapp.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.name.weatherapp.CitySelect;
import com.example.name.weatherapp.ClassConvert;
import com.example.name.weatherapp.GetJsonApi;
import com.example.name.weatherapp.ModelWeatherList;
import com.example.name.weatherapp.R;
import com.example.name.weatherapp.WeatherListAdapter;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FiveDayFragment extends Fragment {

    private ImageView icon;
    private TextView city;
    private TextView weather;
    private TextView time;
    private TextView date;
    private TextView cloud;
    private TextView wind;
    private TextView temp;

    private String url = "";

    private List<ModelWeatherList> modelWeatherLists;


    private CitySelect citySelect;

    protected RecyclerView recycleList;
    protected WeatherListAdapter mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;

    protected WeatherListAdapter.OnItemClickListener listener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        citySelect = new CitySelect(getActivity());

        url = "http://api.openweathermap.org/data/2.5/forecast?id=" + citySelect.getCityName() + "&APPID=70fcd886ab9d9eb1c5502925f12c97a2";

        new FiveDayWeather(new GetJsonApi()).execute(url);

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getContext())
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_five_day, container, false);

        recycleList = (RecyclerView) view.findViewById(R.id.recycleListElement);

        listener = new WeatherListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ModelWeatherList item) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
//
                LayoutInflater inflater = getActivity().getLayoutInflater();

                View dialogView = inflater.inflate(R.layout.fragment_one_day, null);

                alertDialog.setView(dialogView)
                        .setTitle("Weather Dialog Full Info");

                temp = (TextView) dialogView.findViewById(R.id.temperature);
                wind = (TextView) dialogView.findViewById(R.id.winds_speed);
                cloud = (TextView) dialogView.findViewById(R.id.clouds);
                date = (TextView) dialogView.findViewById(R.id.date);
                time = (TextView) dialogView.findViewById(R.id.time);
                weather = (TextView) dialogView.findViewById(R.id.weather);
                city = (TextView) dialogView.findViewById(R.id.city_name);

                icon = (ImageView) dialogView.findViewById(R.id.weatherImage);

                temp.setText(String.valueOf(item.getTempNow()));
                wind.setText(item.getWindsNow());
                cloud.setText(item.getCloudsNow());
                date.setText(item.getDateNow());
                time.setText(item.getTimeNow());
                weather.setText(item.getWeatherNow());
                city.setText(item.getCityName());

                ImageLoader.getInstance().displayImage(item.getImageNow(), icon);

                alertDialog.create();
                alertDialog.show();
            }
        };

        mLayoutManager = new LinearLayoutManager(getActivity());
        recycleList.setLayoutManager(mLayoutManager);

        mAdapter = new WeatherListAdapter(modelWeatherLists, listener);
        recycleList.setAdapter(mAdapter);

        return view;
    }
    public class FiveDayWeather extends AsyncTask<String, String, Void>{

        private GetJsonApi getJsonApi;
        private String cityName = "";
        private String tempNow;
        private String cloudsNow = "";
        private String windsNow = "";
        private String dateNow = "";
        private String timeNow = "";
        private String weatherNow = "";
        private String imageNow = "";


        public FiveDayWeather(GetJsonApi getJsonApi){

            this.getJsonApi = getJsonApi;
        }

        @Override
        protected Void doInBackground(String... strings) {

            modelWeatherLists = new ArrayList<>();

            String json = getJsonApi.connect(strings[0]);
            try {
                JSONObject jsonObject = new JSONObject(json);

                JSONArray jsonArray = jsonObject.getJSONArray("list");

                JSONObject cityObject = jsonObject.getJSONObject("city");

                for(int i = 0; i < jsonArray.length(); i++){

                    JSONObject listObject = jsonArray.getJSONObject(i);

                    if(i == 0 || i == 8 || i == 16 || i == 24 || i == 32){

                        JSONObject mainObject = listObject.getJSONObject("main");
                        JSONObject cloudsObject = listObject.getJSONObject("clouds");
                        JSONObject windsObject = listObject.getJSONObject("wind");

                        cityName = cityObject.getString("name");
                        tempNow = String.valueOf(ClassConvert.tempKelvinConvert(mainObject.getString("temp")));
                        cloudsNow = cloudsObject.getString("all");
                        windsNow = windsObject.getString("speed");
                        dateNow = ClassConvert.unixConvert(listObject.getString("dt"),true);
                        timeNow = ClassConvert.unixConvert(listObject.getString("dt"),false);

                        for(int j  = 0; j < listObject.getJSONArray("weather").length(); j++){
                            weatherNow = listObject.getJSONArray("weather").getJSONObject(j).getString("main");
                            imageNow = listObject.getJSONArray("weather").getJSONObject(j).getString("icon");
                        }


                        ModelWeatherList setModelList = new ModelWeatherList(cityName,tempNow,cloudsNow,windsNow,dateNow,
                                                                                timeNow,weatherNow,imageNow);

                       modelWeatherLists.add(setModelList);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            mAdapter.notifyDataSetChanged();
        }
    }
}

