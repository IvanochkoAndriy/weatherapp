package com.example.name.weatherapp.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.name.weatherapp.CitySelect;
import com.example.name.weatherapp.ClassConvert;
import com.example.name.weatherapp.GetJsonApi;
import com.example.name.weatherapp.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class OneDayFragment extends Fragment {
    private String url = "";

    private TextView cityName;
    private TextView myDate;
    private TextView myTime;
    private TextView windSpeed;
    private TextView tempView;
    private TextView cloudsView;
    private TextView weatherView;
    private TextView riseSun;
    private TextView setSun;
    private ImageView imageWeather;

    private CitySelect citySelect;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        citySelect = new CitySelect(getActivity());

        url = "http://api.openweathermap.org/data/2.5/weather?id=" + citySelect.getCityName() + "&APPID=70fcd886ab9d9eb1c5502925f12c97a2";

        new OneDayWeaher(new GetJsonApi()).execute(url);

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getContext())
            .defaultDisplayImageOptions(defaultOptions)
            .build();
        ImageLoader.getInstance().init(config);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_one_day, container, false);
        cityName = (TextView)view.findViewById(R.id.city_name);
        myDate = (TextView)view.findViewById(R.id.date);
        myTime = (TextView)view.findViewById(R.id.time);
        windSpeed = (TextView)view.findViewById(R.id.winds_speed);
        cloudsView = (TextView)view.findViewById(R.id.clouds);
        tempView = (TextView)view.findViewById(R.id.temperature);
        weatherView = (TextView)view.findViewById(R.id.weather);
        setSun = (TextView)view.findViewById(R.id.time_set_sun);
        riseSun = (TextView)view.findViewById(R.id.time_rise_sun);
        imageWeather = (ImageView)view.findViewById(R.id.weatherImage);

        return view;
    }


    private class OneDayWeaher extends AsyncTask<String,String,Map>{

        private GetJsonApi getJsonApi;

        public OneDayWeaher(GetJsonApi getJsonApi) {
            this.getJsonApi = getJsonApi;
        }

        @Override
        protected Map doInBackground(String... strings) {

            String json = getJsonApi.connect(strings[0]);
            String weatherName = "";
            String cityName = "";
            String windSpeed = "";
            String cloudsNow = "";
            String myDate = "";
            String myTime = "";
            String setSun = "";
            String riseSun = "";
            String image = "";

            String tempNumb;

            try {
                JSONObject jsonObject = new JSONObject(json);
                JSONArray jsonArray = jsonObject.getJSONArray("weather");

                JSONObject windsObject = jsonObject.getJSONObject("wind");
                JSONObject cloudObject = jsonObject.getJSONObject("clouds");
                JSONObject mainObject = jsonObject.getJSONObject("main");
                JSONObject sysObject = jsonObject.getJSONObject("sys");

                cityName = jsonObject.getString("name");

                DateFormat df = new SimpleDateFormat("HH:mm");
                String timeConvert = df.format(Calendar.getInstance().getTime());


                myDate = ClassConvert.unixConvert(jsonObject.getString("dt"),true);
                myTime = timeConvert;
                riseSun = ClassConvert.unixConvert(sysObject.getString("sunrise"), false);
                setSun = ClassConvert.unixConvert(sysObject.getString("sunset"), false);

                tempNumb = mainObject.getString("temp");
                tempNumb = String.valueOf(ClassConvert.tempKelvinConvert(tempNumb));

                Map<String, Object> map =  null;
                for (int i = 0; i < jsonArray.length(); i++){
                    JSONObject object = jsonArray.getJSONObject(i);

                    map = new HashMap<>();

                    weatherName = object.getString("main");
                    windSpeed = windsObject.getString("speed");
                    cloudsNow = cloudObject.getString("all");
                    image = object.getString("icon");

                    map.put("weatherName", weatherName);
                    map.put("windSpeed", windSpeed + " m/s");
                    map.put("cloudsNow", cloudsNow + " %");
                    map.put("tempNumb", tempNumb + "°C");
                    map.put("cityName", cityName);
                    map.put("myDate", myDate);
                    map.put("myTime", myTime);
                    map.put("riseSun", riseSun);
                    map.put("setSun", setSun);
                    map.put("image", "http://openweathermap.org/img/w/" + image + ".png");
                }

                return map;
            }catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Map  result) {
            cityName.setText(result.get("cityName").toString());
            myDate.setText(result.get("myDate").toString());
            myTime.setText(result.get("myTime").toString());
            windSpeed.setText(result.get("windSpeed").toString());
            cloudsView.setText(result.get("cloudsNow").toString());
            tempView.setText(result.get("tempNumb").toString());
            weatherView.setText(result.get("weatherName").toString());
            riseSun.setText(result.get("riseSun").toString());
            setSun.setText(result.get("setSun").toString());
            ImageLoader.getInstance().displayImage(result.get("image").toString(), imageWeather);

            super.onPostExecute(result);
        }
    }
}
