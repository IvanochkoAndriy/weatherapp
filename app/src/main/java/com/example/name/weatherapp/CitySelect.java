package com.example.name.weatherapp;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class CitySelect {

    private Activity activity;

    public static String ID_DROGOBYCH = "709611";
    public static String ID_LVIV = "702550";
    public static String ID_TRUSKAVETC = "691179";

    public CitySelect(Activity activity){
        this.activity = activity;

    }

    public void setCityName(String cityId){
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putString(String.valueOf(R.string.shared_save), cityId).commit();
    }

    public String getCityName(){

        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);

        String highScore = sharedPref.getString(String.valueOf(R.string.shared_save), ID_DROGOBYCH);

        return highScore;
    }

    public void setRadio(RadioGroup cityGroup, int checkedId, Activity activity){
        RadioButton radioButton = (RadioButton) cityGroup.findViewById(checkedId);
        int checkedIndex = cityGroup.indexOfChild(radioButton);

        SharedPreferences sharedPreferences = activity.getSharedPreferences("MY_SHARED_PREF", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(String.valueOf(R.string.radioSaveString), checkedIndex).commit();
    }

    public int getRadio(){

        SharedPreferences sharedPreferences = activity.getSharedPreferences("MY_SHARED_PREF", Context.MODE_PRIVATE);
        int savedRadioIndex = sharedPreferences.getInt(String.valueOf(R.string.radioSaveString), 0);

        return savedRadioIndex;
    }

}
