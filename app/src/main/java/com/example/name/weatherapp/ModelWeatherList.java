package com.example.name.weatherapp;

/**
 * Created by Name on 20.12.2017.
 */

public class ModelWeatherList {

    private String cityName = "";
    private String tempNow;
    private String cloudsNow = "";
    private String windsNow = "";
    private String dateNow = "";
    private String timeNow = "";
    private String weatherNow = "";
    private String imageNow = "";


    public ModelWeatherList(String cityName, String tempNow, String cloudsNow, String windsNow,
                       String dateNow, String timeNow, String weatherNow, String imageNow) {

        this.cityName = cityName;
        this.tempNow = tempNow;
        this.cloudsNow = cloudsNow;
        this.windsNow = windsNow;
        this.dateNow = dateNow;
        this.timeNow = timeNow;
        this.weatherNow = weatherNow;
        this.imageNow = imageNow;
    }

    public String getCityName() {
        return cityName;
    }

    public String getTempNow() {
        return tempNow + "°C";
    }

    public String getCloudsNow() {
        return cloudsNow + "%";
    }

    public String getWindsNow() {
        return windsNow + " m/s";
    }

    public String getDateNow() {
        return dateNow;
    }

    public String getTimeNow() {
        return timeNow;
    }

    public String getWeatherNow() {
        return weatherNow;
    }

    public String getImageNow() {
        return "http://openweathermap.org/img/w/" + imageNow + ".png";
    }


}
